'use strict';

function toggleFunc() {
  var that, toggledCached;
  $('.page-root').append('<div class="page-root__overlay">');
  $('[data-toggle]').click(function () {
    that = $(this);
    if (!that.hasClass('_toggled')) {
      toggledCached = that.data('toggle') + ', ' + this.className;
    }
    that.toggleClass('_toggled');
    $(toggledCached).toggleClass('_toggled');
  });
  $('.page-root__overlay').click(function () {
    that.removeClass('_toggled');
    $(toggledCached).removeClass('_toggled');
  });
  $(document).keyup(function (e) {
    if (toggledCached !== undefined) {
      if (e.keyCode === 27) {
        that.removeClass('_toggled');
        $(toggledCached).removeClass('_toggled');
      }
    }
  });
}

function clientsCarousel() {
  $('.js-slider-clients').owlCarousel({
    dots: false,
    items: 4,
    responsive: {
      0: {
        items: 2,
        nav: false
      },
      740: {
        items: 4
      }
    }
  });
}

$(function () {
  toggleFunc();
  clientsCarousel();
});
